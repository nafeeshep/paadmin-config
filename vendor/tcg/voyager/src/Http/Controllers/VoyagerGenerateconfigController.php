<?php

namespace TCG\Voyager\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use TCG\Voyager\Facades\Voyager;

class VoyagerGenerateconfigController extends Controller
{
    public function index()
    {
        // Check permission
        $this->authorize('browse', Voyager::model('Setting'));
        return Voyager::view('voyager::generateconfig.index');
    }
    public function postconfig(Request $request)
    {
        // Check permission
        $this->authorize('browse', Voyager::model('Setting'));
        if ($request->isMethod('post')){    
            $data = $request->all();
            $key_file = storage_path('key.txt');
            $handle = fopen($key_file, 'w') or die('Cannot open file:  '.$key_file);
            $pass_phrase=md5($data['passphrase']);
            fwrite($handle, $data['passphrase']);
            fclose($handle);
            $config_file = storage_path('config.txt');
            $handle = fopen($config_file, 'w') or die('Cannot open file:  '.$config_file);
            unset($data['passphrase'],$data['_token']);
            $encrypted_data = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256,$pass_phrase , json_encode($data), MCRYPT_MODE_ECB));
            fwrite($handle, $encrypted_data);   
            fclose($handle);    
            echo 'Files created !!';
        }
       
    }
    public function downloadkey(){
        $key_file = storage_path('key.txt');
        return response()->download($key_file);
    }
    public function downloadconfig(){
        $config_file = storage_path('config.txt');
        return response()->download($config_file);
    }

}
