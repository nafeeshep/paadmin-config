@extends('voyager::master')

@section('content')
    <div class="page-content">
        @include('voyager::alerts')
        <div class="analytics-container">
            
                <p style="border-radius:4px; padding:20px; background:#fff; margin:0; color:#999; text-align:center;">
                Business Intelligence dashboard : To manage operational efficiency and resource management
                </p>

        </div>
    </div>
@stop