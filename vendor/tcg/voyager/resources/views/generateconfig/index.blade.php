@extends('voyager::master')

@section('page_title', 'Generate Patient Assist Configuration File')

@section('css')
    <style>
        .panel-actions .voyager-trash {
            cursor: pointer;
        }
        .panel-actions .voyager-trash:hover {
            color: #e94542;
        }
        .settings .panel-actions{
            right:0px;
        }
        .panel hr {
            margin-bottom: 10px;
        }
        .panel {
            padding-bottom: 15px;
        }
        .sort-icons {
            font-size: 21px;
            color: #ccc;
            position: relative;
            cursor: pointer;
        }
        .sort-icons:hover {
            color: #37474F;
        }
        .voyager-sort-desc {
            margin-right: 10px;
        }
        .voyager-sort-asc {
            top: 10px;
        }
        .page-title {
            margin-bottom: 0;
        }
        .panel-title code {
            border-radius: 30px;
            padding: 5px 10px;
            font-size: 11px;
            border: 0;
            position: relative;
            top: -2px;
        }
        .modal-open .settings  .select2-container {
            z-index: 9!important;
            width: 100%!important;
        }
        .new-setting {
            text-align: center;
            width: 100%;
            margin-top: 20px;
        }
        .new-setting .panel-title {
            margin: 0 auto;
            display: inline-block;
            color: #999fac;
            font-weight: lighter;
            font-size: 13px;
            background: #fff;
            width: auto;
            height: auto;
            position: relative;
            padding-right: 15px;
        }
        .settings .panel-title{
            padding-left:0px;
            padding-right:0px;
        }
        .new-setting hr {
            margin-bottom: 0;
            position: absolute;
            top: 7px;
            width: 96%;
            margin-left: 2%;
        }
        .new-setting .panel-title i {
            position: relative;
            top: 2px;
        }
        .new-settings-options {
            display: none;
            padding-bottom: 10px;
        }
        .new-settings-options label {
            margin-top: 13px;
        }
        .new-settings-options .alert {
            margin-bottom: 0;
        }
        #toggle_options {
            clear: both;
            float: right;
            font-size: 12px;
            position: relative;
            margin-top: 15px;
            margin-right: 5px;
            margin-bottom: 10px;
            cursor: pointer;
            z-index: 9;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        .new-setting-btn {
            margin-right: 15px;
            position: relative;
            margin-bottom: 0;
            top: 5px;
        }
        .new-setting-btn i {
            position: relative;
            top: 2px;
        }
        textarea {
            min-height: 120px;
        }
        textarea.hidden{
            display:none;
        }

        .voyager .settings .nav-tabs{
            background:none;
            border-bottom:0px;
        }

        .voyager .settings .nav-tabs .active a{
            border:0px;
        }

        .select2{
            width:100% !important;
            border: 1px solid #f1f1f1;
            border-radius: 3px;
        }

        .voyager .settings input[type=file]{
            width:100%;
        }

        .settings .select2{
            margin-left:10px;
        }

        .settings .select2-selection{
            height: 32px;
            padding: 2px;
        }

        .voyager .settings .nav-tabs > li{
            margin-bottom:-1px !important;
        }

        .voyager .settings .nav-tabs a{
            text-align: center;
            background: #f8f8f8;
            border: 1px solid #f1f1f1;
            position: relative;
            top: -1px;
            border-bottom-left-radius: 0px;
            border-bottom-right-radius: 0px;
        }

        .voyager .settings .nav-tabs a i{
            display: block;
            font-size: 22px;
        }

        .tab-content{
            background:#ffffff;
            border: 1px solid transparent;
        }

        .tab-content>div{
            padding:10px;
        }

        .settings .no-padding-left-right{
            padding-left:0px;
            padding-right:0px;
        }

        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover{
            background:#fff !important;
            color:#62a8ea !important;
            border-bottom:1px solid #fff !important;
            top:-1px !important;
        }

        .nav-tabs > li a{
            transition:all 0.3s ease;
        }


        .nav-tabs > li.active > a:focus{
            top:0px !important;
        }

        .voyager .settings .nav-tabs > li > a:hover{
            background-color:#fff !important;
        }
    </style>
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-lock"></i> Generate Patient Assist Configuration File
    </h1>
@stop

@section('content')
    <div class="container-fluid">
        @include('voyager::alerts')
        @if(config('voyager.show_dev_tips'))
        <div class="alert alert-info">
            <strong>{{ __('voyager.generic.how_to_use') }}:</strong>
            <p>Please fill in the details and click the generate file button. A zip will get downloaded and you can find the encrypted configuration file and key file in that. Please put the files in the respective configuration folders of Patient Assist Application</p>
        </div>
        @endif

        <div class="panel-body" style="padding-top:0px;"><div class="row">
    <form id="generateconfig" >
    {{ csrf_field() }}
    <h4 class="heading"><strong>Encryption </strong> Details <span></span></h4>
  <div class="form-group">
    <label for="passphrase">Pass Phrase:</label>
    <input type="text" name="passphrase" class="form-control" id="passphrase" value={{uniqid('', true)}} readonly>
  </div>
    <h4 class="heading"><strong>Deployment </strong> Details <span></span></h4>
  <div class="form-group">
    <label for="environment">Select Environment:</label>
    <select class="form-control" name="environment" id="environment">
    <option>Development</option>
    <option selected>Staging</option>
    <option>Production</option>
    <option>Testing</option>
  </select>
  </div>
  <div class="form-group">
    <label for="domain">Domain Address:</label>
    <input type="domain" name="domain" class="form-control" id="domain" value="{{setting('setup.url')}}">
  </div>
  <div class="form-group">
    <label for="domain">Time Zone:</label>
    <input type="text" name="time_zone" class="form-control" id="time_zone" value="{{setting('setup.time_zone')}}">
  </div>
  <h4 class="heading"><strong>Database </strong> Details <span></span></h4>
  <div class="form-group">
    <label for="dbhost">DATABASE HOST:</label>
    <input type="text" name="dbhost" class="form-control" id="dbhost" value="{{setting('setup.dbhost')}}">
  </div>
  <div class="form-group">
    <label for="dbname">DATABASE NAME:</label>
    <input type="text" name="dbname" class="form-control" id="dbname" value="{{setting('setup.dbname')}}">
  </div>
  <div class="form-group">
    <label for="dbuser">DATABASE USER:</label>
    <input type="text" name="dbuser" class="form-control" id="dbuser" value="{{setting('setup.dbuser')}}">
  </div>
  <div class="form-group">
    <label for="dbpassword">DATABASE PASSWORD:</label>
    <input type="text" name="dbpassword" class="form-control" id="dbpassword" value="{{setting('setup.dbpassword')}}">
  </div>
  <h4 class="heading"><strong>Secondary Database </strong> Details <span></span></h4>
  <div class="form-group">
    <label for="dbhost2">DATABASE HOST:</label>
    <input type="text" name="dbhost2" class="form-control" id="dbhost2" value="{{setting('setup.dbhost2')}}">
  </div>
  <div class="form-group">
    <label for="dbname2">DATABASE NAME:</label>
    <input type="text" name="dbname2" class="form-control" id="dbname2" value="{{setting('setup.dbname2')}}">
  </div>
  <div class="form-group">
    <label for="dbuser2">DATABASE USER:</label>
    <input type="text" name="dbuser2" class="form-control" id="dbuser2" value="{{setting('setup.dbuser2')}}">
  </div>
  <div class="form-group">
    <label for="dbpassword2">DATABASE PASSWORD:</label>
    <input type="text" name="dbpassword2" class="form-control" id="dbpassword2" value="{{setting('setup.dbpassword2')}}">
  </div>
  <h4 class="heading"><strong>SMTP </strong> Details <span></span></h4>
  <div class="form-group">
    <label for="smtphost">SMTP HOST:</label>
    <input type="text" name="smtphost" class="form-control" id="smtphost" value="{{setting('setup.smtphost')}}">
  </div>
  <div class="form-group">
    <label for="smtpport">SMTP PORT:</label>
    <input type="text" name="smtpport" class="form-control" id="smtpport" value="{{setting('setup.smtpport')}}">
  </div>
  <div class="form-group">
    <label for="smtpuser">SMTP USER:</label>
    <input type="text" name="smtpuser" class="form-control" id="smtpuser" value="{{setting('setup.smtpuser')}}">
  </div>
  <div class="form-group">
    <label for="smtppassword">SMTP PASSWORD:</label>
    <input type="text" name="smtppassword" class="form-control" id="smtppassword" value="{{setting('setup.smtppassword')}}">
  </div>
  <h4 class="heading"><strong>Email </strong> Addresses <span></span></h4>
  <div class="form-group">
    <label for="fromemail">Registration From Address:</label>
    <input type="text" name="fromemail" class="form-control" id="fromemail" value="{{setting('setup.fromemail')}}">
  </div>
  <div class="form-group">
    <label for="fromemail">Alarm Mail From Address:</label>
    <input type="text" name="alarmfromemail" class="form-control" id="alarmfromemail" value="{{setting('setup.alarmfromemail')}}">
  </div>
  
  <div class="form-group">
    <label for="toemail">Registration To Address:</label>
    <input type="text" name="toemail" class="form-control" id="toemail" value="{{setting('setup.toemail')}}">
  </div>
  <div class="form-group">
    <label for="supportemail">Support To Address:</label>
    <input type="text" name="supportemail" class="form-control" id="supportemail" value="{{setting('setup.supportemail')}}">
  </div>
  <h4 class="heading"><strong>Folder </strong> Paths <span></span></h4>
  <div class="form-group">
    <label for="fupload">File Upload Folder</label>
    <input type="text" name="fupload" class="form-control" id="fupload" value="{{setting('setup.fupload')}}">
  </div>
  <div class="form-group">
    <label for="relativepath1">File Upload Relative Path 1</label>
    <input type="text" name="relativepath1" class="form-control" id="relativepath1" value="{{setting('setup.fupload')}}">
  </div>
  <div class="form-group">
    <label for="relativepath2">File Upload Relative Path 2</label>
    <input type="text" name="relativepath2" class="form-control" id="relativepath2" value="{{setting('setup.fupload')}}">
  </div>
  <div class="form-group">
    <label for="oncocda">Onco CDA Folder:</label>
    <input type="text" name="oncocda" class="form-control" id="oncocda" value="{{setting('setup.oncocda')}}">
  </div>
  <h4 class="heading"><strong>SMS </strong> Details <span></span></h4>
  <div class="form-group">
    <label for="sms_gateway">SMS Gateway:</label>
    <input type="text" name="sms_gateway" class="form-control" id="sms_gateway" value="{{setting('setup.sms_gateway')}}">
  </div>
  <div class="form-group">
    <label for="sms_user">SMS User:</label>
    <input type="text" name="sms_user" class="form-control" id="sms_user" value="{{setting('setup.sms_user')}}">
  </div>
  <div class="form-group">
    <label for="sms_pass">SMS Password:</label>
    <input type="text" name="sms_pass" class="form-control" id="sms_pass" value="{{setting('setup.sms_pass')}}">
  </div>
  <h4 class="heading"><strong>PEM </strong> File <span></span></h4>
  <div class="form-group">
    <label for="pemfile">File Name</label>
    <input type="text" name="pemfile" class="form-control" id="pemfile" value="{{setting('setup.pemfile')}}">
  </div>
  <button type="button"  id="downloadkey" class="btn btn-success">Download Files</button> 
</form>
    </div>
    </div>
   </div>
   

<!-- Modal -->
<div id="fileDownload" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Download Files</h4>
      </div>
      <div class="modal-body">
        <p>Please download the configuration file and key file.</p>
       <a href="{{route('voyager.downloadkey')}}"> <button type="button"  class="btn btn-success">Download Key</button></a> <a href="{{route('voyager.downloadconfig')}}"> <button type="button" id="downloadconfig"  class="btn btn-danger">Download Cofiguration File</button></a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@stop
@section('javascript')
    <script>
    $("#downloadkey").click(function(e) {
        data= $('form').serialize();
        $.post( "{{route('voyager.postconfig')}}", data)
            .done(function( data ) {
                $('#fileDownload').modal('show'); 
        });
    });
    </script>
@stop