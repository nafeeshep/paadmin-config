<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
$file = basename(__FILE__, '.php');
require_once("./config/setup.php");
require_once("./library/functions.php");
require_once("./library/languageswitch.php");
 
$filename = './files/ip_address.txt';
//echo get_client_ip(). '<br>';
// echo getHostByName(getHostName());
if (!isset($_SESSION['CURR_USR_UID'])) {
   $current_user_id =0;
}else if($_SESSION['CURR_USR_UID']>0 && $_SESSION['CURR_USR_TYPE'] == 'admin'){
     header("Location: overview.php");
}else if($_SESSION['CURR_USR_UID']>0 && $_SESSION['CURR_USR_TYPE'] == 'institution'){
     header("Location: institution_overview.php");
}

$arr = array(
    'status' => 0,
    'msg' => "FEHLER! Falsche E-Mail/Benutzername oder Passwort"
);
$current_user_id =0;
$ip_address_array = file($filename, FILE_IGNORE_NEW_LINES);
if (isset($_POST['user_name'])&&isset($_POST['user_password']) ) {
    $Uname = trim($_POST['user_name']);
    $Upassword = trim($_POST['user_password']);  
    checkLogin('patient_assist', $Uname, $Upassword); 
} 
//echo json_encode($arr);


function checkLogin($app,$Uname,$Upassword) {
    global $db,$arr,$ip_address_array;
    $password = $Upassword;
 
    $validuser = $db->where('user_name',$Uname)->orWhere('user_email',$Uname)->get('user_details');
    if($validuser){
         //  $salt = $validuser[0]['password_salt'];
          //$hash= @crypt($password,$salt);
           $hash= password_verify($password,$validuser[0]['user_password']);
           $user_type = $validuser[0]['user_type'];
           $u_status = $validuser[0]['user_status'];
           $closed_user = $validuser[0]['is_closed'];
           $locked_user = $validuser[0]['is_locked'];
           $unlock_time = $validuser[0]['unlock_time'];
           $failed_attemptflag = $validuser[0]['total_failed_attempts'] + 1; 
           $ip_check_ok = 0;
           $deactive_till_date = $validuser[0]['user_deactivated_till'];
           if(isset($u_status) && $u_status == 0){                                            
                $arr["status"] = 5;
                $arr["msg"] = "USER DEACTIVATED";                
            }
            if(isset($u_status) && $u_status == 1 && date($deactive_till_date) > date("d.m.Y")){                                            
                $arr["status"] = 9;
                $arr["msg"] = "USER DEACTIVATED till" .$deactive_till_date;                
            }
           if(isset($closed_user) && $closed_user == 1){                                            
                $arr["status"] = 7;
                $arr["msg"] = "Account Closed";                
            }
            if(isset($failed_attemptflag) && $failed_attemptflag == 4){  
                $arr["status"] = 6;
                $arr["heading"] = "failed atempt6";
                $arr["msg"] = "failed atempt6";   
            }
            if($locked_user == 1 && strtotime($unlock_time) > strtotime( date("Y-m-d H:i:s"))) {
                $arr["status"] = 8;
                $arr["msg"] = ceil(abs(strtotime($unlock_time) - strtotime( date("Y-m-d H:i:s")))/60);;
            }


        If ($hash == true) {
                $ip_adr = get_client_ip();
        
              if($user_type=='admin'){
                 if (in_array($ip_adr, $ip_address_array)){
                  $ip_check_ok = 1;  
                 }
                else{
                     foreach($ip_address_array as $ipInFile){
                        if (strpos( $ipInFile, '-' ) !== false){
                          $range= explode("-",$ipInFile);
                          if (ip2long($ip_adr) >= ip2long(trim($range[0])) && ip2long($ip_adr) <= ip2long(trim($range[1]))) {
                              $ip_check_ok = 1;
                          }
                        }
                   
                    }
                }
              }else if($user_type=='institution'){
                  $ip_check_ok = 1;
              }

              if($u_status == 2 || ($u_status == 1 && date($deactive_till_date) <= date("d.m.Y")) ){
              
                //check ip address of client
              if ($ip_check_ok == 1 ) {
                    $user_id = $validuser[0]['user_id'];
                    $user_name = $validuser[0]['user_profile_name'];

                    $curr_u_id = array(
                        "user_active" => 1,
                        "u_id" => $user_id,
                        "u_type" => $validuser[0]['user_type'],
                        "ip_adr" => $ip_adr
                    );

                    $usr_act = $db->insert("user_active", $curr_u_id);
                 
                    $_SESSION['CURR_USR_UID'] = $user_id;
                    $_SESSION['CURR_USR_TYPE'] = $user_type;
                    $_SESSION['CURR_USR_NAME'] = $user_name;
                    $_SESSION['LANGUAGE'] =$validuser[0]['language'];;
                    $_SESSION['USR_ACT_ID'] = $usr_act;
                    $arr["msg"] = "Login Sucessfull";
                     $updsuccess_attempts = array(
                        "is_locked" => 0,
                        "is_closed" => 0,
                        "total_failed_attempts" => 0,
                        "unlock_time" => "0000-00-00 00:00:00",
                        "user_status" => '2'
                    );
                    $db->where('user_id', $user_id)->update("user_details", $updsuccess_attempts);
                    if($user_type=='admin'){
                     header('Location: overview.php');
                    }else if($user_type=='institution'){
                        header('Location: institution_overview.php');
                    }
                   }else{
                        $arr = array(
                                'status' => 4,
                                'msg' => "Invalid IP Address!"
                            );
                  }
              }else{
                $arr = array(
                            'status' => 5,
                            'msg' => "User not active!"
                        );
              }
        } else {  
            $current_time=date('Y-m-d H:i:s');
            $failed_attemptflag = $validuser[0]['total_failed_attempts'] + 1;             
              if($failed_attemptflag > 7){
                    $failed_attemptflag=7;
                }
                 $loginfail_rules = $db->where("fail_count", $failed_attemptflag)->get('login_fail_rules');
                 if($loginfail_rules[0]['fail_action_type']=='do_nothing'){
                    // do nothing
                }else if($loginfail_rules[0]['fail_action_type']=='minutes_lock'){
                    $expire_stamp = date('Y-m-d H:i:s',strtotime('+'.$loginfail_rules[0]['lock_minutes'].' minutes',strtotime($current_time)));
                    $upd = $db->where("user_id", $validuser[0]['user_id'])->update("user_details", array("is_locked" => 1,"unlock_time" => $expire_stamp));
                } else {
                    $upd = $db->where("user_id", $validuser[0]['user_id'])->update("user_details", array("is_closed" => 1,"is_locked" => 0,"unlock_time" => "2000-01-01 00:00:00"));
                }
                 $upd = $db->where("user_id", $validuser[0]['user_id'])->update("user_details", array("total_failed_attempts" => $failed_attemptflag));
                 $ip = get_client_ip();
                 if ($ip != 'UNKNOWN') {
                    $in_addr = inet_pton($ip);
                } else {
                    $in_addr = '';
                }
                if($loginfail_rules[0]['fail_action_type']=='minutes_lock'){
                    
                }
                $failed_attpt = array(
                        "user_id" => 1,
                        "attempt_ip" => $in_addr,
                        "attempt_time" => $current_time,
                        "attempt_mode" => "web"                       
                    );

                    $usr_act = $db->insert("failed_attempt_logs", $failed_attpt);
                    $arr = array(
                    'status' => 3,
                    'msg' => "PASSWORD WRONG"
                ); 
        }
    }else{
         $arr = array(
                    'status' => 2,
                    'msg' => "INVALID USER"
                );  
    }
}

  function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
$langGlobal = getPageContent($file);

require './views/template/header.php';
require './views/' . $file . "_view.php";
require './views/template/footer.php';
?>